# Bible App
This app prints random Bible verses if no arguments are specified, or the verse/range of
verses provided by the user. You can use 3 letter book abbreviations. Don't put any spaces
in the book name.
Example commands:
```bash
bible
bible genesis 1:1
bible gen 1 1
bible gen 1:1-5
bible gen 1 1-5
bible 1timothy 2:1
```
By adding the bible command to your .bashrc, you will be served a random bible verse every
time you open your terminal.

# Installation
Linux x86-64:
```bash
mkdir ~/bible
cd ~/bible
curl https://gitlab.com/api/v4/projects/46775326/packages/generic/bible_linux_x86-64/1.0.0/bible --output bible
chmod +x bible
echo "export PATH=\"$(pwd):\$PATH\"" >> ~/.bashrc
echo "bible" >> ~/.bashrc
source ~/.bashrc
```

Mac x86-64:
```bash
mkdir ~/bible
cd ~/bible
curl https://gitlab.com/api/v4/projects/46775326/packages/generic/bible_mac_x86-64/1.0.0/bible --output bible
chmod +x bible
echo "export PATH=\"$(pwd):\$PATH\"" >> ~/.bashrc
echo "bible" >> ~/.bashrc
source ~/.bashrc
```

Mac ARM64:
```bash
mkdir ~/bible
cd ~/bible
curl https://gitlab.com/api/v4/projects/46775326/packages/generic/bible_mac_ARM64/1.0.0/bible --output bible
chmod +x bible
echo "export PATH=\"$(pwd):\$PATH\"" >> ~/.bashrc
echo "bible" >> ~/.bashrc
source ~/.bashrc
```

Windows x86-64:
```bash
mkdir ~/bible
cd ~/bible
curl https://gitlab.com/api/v4/projects/46775326/packages/generic/bible_win_x86-64/1.0.0/bible.exe --output bible.exe
echo "export PATH=\"$(pwd):\$PATH\"" >> ~/.bashrc
echo "bible" >> ~/.bashrc
source ~/.bashrc
```

# Sources
Inspired word of God. 
(And this guy who put the JSONs on Github: https://github.com/kenyonbowers/Bible-JSON)
