import os
import json
# Bible from kenyonbowers with red words/italics
bible_dir = "Bible-JSON/JSON"
books = os.listdir(bible_dir)
bible = {}
book_ids = {}
for book in books:
    chap_dir = os.path.join(bible_dir, book)
    chapters = os.listdir(chap_dir)
    bible[book] = {}
    for chapter in chapters:
        bible[book][chapter[:chapter.find('.')]] = {}
        with open(os.path.join(chap_dir, chapter), 'r+', encoding='utf-8') as f:
            verses = json.load(f)["verses"]
            if verses[0]['book_id'] not in book_ids.keys():
                book_ids[verses[0]['book_id']] = book
            for verse in verses:
                bible[book][chapter[:chapter.find('.')]][verse["verse"]] = verse["text"].replace('\u00b6 ', '')

with open("src/kjv_bible.json", 'w+', encoding='utf-8') as f:
    json.dump(bible, f)

with open("abbrev.txt", "w+") as f:
    f.write("static abbrevs: [(&str, &str); 66] = [\n")
    for abb, book in book_ids.items():
        f.write(f"    (\"{abb}\", \"{book}\"),\n")
    f.write("];")

