#[cfg(test)]
pub mod tests {
    use std::fs::read;
    use colored::{Color, Styles};
    use json::parse;
    use crate::bible::{get_styled_text, Bible, InputError, InputOption};
    #[test]
    fn entire_text_matches() {
        let content = String::from_utf8(read("src/kjv_bible.json").unwrap()).unwrap();
        let bible = parse(&content).unwrap();
        for book in bible.entries() {
            for chapter in book.1.entries() {
                for verse in chapter.1.entries() {
                    let mut clean_text = verse.1.to_string();
                    clean_text = clean_text.replace("<span style=\"color:red;\">", "");
                    clean_text = clean_text.replace("</span>", "");
                    clean_text = clean_text.replace("<em>", "");
                    clean_text = clean_text.replace("</em>", "");
                    let text = get_styled_text(&verse.1.to_string());
                    let mut styled_verse = String::new();
                    for item in text {
                        styled_verse.push_str(&item.0);
                    }
                    assert_eq!(styled_verse, clean_text);
                }
            }
        }
    }

    #[test]
    fn color_and_multiple_italic() {
        let mut bible_cli = Bible {
            book: Some("John".to_string()),
            chapter: Some("4".to_string()),
            verse: Some("24".to_string()),
            input_option: InputOption::None,
        };
        let res = bible_cli.process().unwrap().unwrap();
        assert_eq!(res.0, "<span style=\"color:red;\">God <em>is</em> a Spirit: and they that worship him must worship <em>him</em> in spirit and in truth.</span>".to_string());
        assert_eq!(res.1[0], ("God ".to_string(), Some(Color::Red), None));
        assert_eq!(res.1[1], ("is".to_string(), Some(Color::Red), Some(Styles::Italic)));
        assert_eq!(res.1[2], (" a Spirit: and they that worship him must worship ".to_string(), Some(Color::Red), None));
        assert_eq!(res.1[3], ("him".to_string(), Some(Color::Red), Some(Styles::Italic)));
        assert_eq!(res.1[4], (" in spirit and in truth.".to_string(), Some(Color::Red), None));
    }

    #[test]
    fn book_with_num() {
        let mut bible_cli = Bible {
            book: Some("1Timothy".to_string()),
            chapter: Some("2".to_string()),
            verse: Some("1".to_string()),
            input_option: InputOption::None,
        };
        let res = bible_cli.process().unwrap().unwrap();
        assert_eq!(res.0, "I exhort therefore, that, first of all, supplications, prayers, intercessions, <em>and</em> giving of thanks, be made for all men;".to_string());
        assert_eq!(res.1[0], ("I exhort therefore, that, first of all, supplications, prayers, intercessions, ".to_string(), None, None));
        assert_eq!(res.1[1], ("and".to_string(), None, Some(Styles::Italic)));
        assert_eq!(res.1[2], (" giving of thanks, be made for all men;".to_string(), None, None));
    }

    #[test]
    fn one_chapter_multiple_verse() {
        let mut bible_cli = Bible {
            book: Some("John".to_string()),
            chapter: Some("4".to_string()),
            verse: Some("24-26".to_string()),
            input_option: InputOption::None,
        };
        let res = bible_cli.process().unwrap().unwrap();
        assert_eq!(res.0, "<span style=\"color:red;\">God <em>is</em> a Spirit: and they that worship him must worship <em>him</em> in spirit and in truth.</span> The woman saith unto him, I know that Messias cometh, which is called Christ: when he is come, he will tell us all things. Jesus saith unto her, <span style=\"color:red;\">I that speak unto thee am <em>he.</em></span>".to_string());
        assert_eq!(res.1[0], ("God ".to_string(), Some(Color::Red), None));
        assert_eq!(res.1[1], ("is".to_string(), Some(Color::Red), Some(Styles::Italic)));
        assert_eq!(res.1[2], (" a Spirit: and they that worship him must worship ".to_string(), Some(Color::Red), None));
        assert_eq!(res.1[3], ("him".to_string(), Some(Color::Red), Some(Styles::Italic)));
        assert_eq!(res.1[4], (" in spirit and in truth.".to_string(), Some(Color::Red), None));
        assert_eq!(res.1[5], (" The woman saith unto him, I know that Messias cometh, which is called Christ: when he is come, he will tell us all things. Jesus saith unto her, ".to_string(), None, None));
        assert_eq!(res.1[6], ("I that speak unto thee am ".to_string(), Some(Color::Red), None));
        assert_eq!(res.1[7], ("he.".to_string(), Some(Color::Red), Some(Styles::Italic)));
    }

    #[test]
    fn multiverse_with_abbreviation() {
        let mut bible_cli = Bible {
            book: Some("Jhn".to_string()),
            chapter: Some("4".to_string()),
            verse: Some("24-26".to_string()),
            input_option: InputOption::None,
        };
        let res = bible_cli.process().unwrap().unwrap();
        assert_eq!(res.0, "<span style=\"color:red;\">God <em>is</em> a Spirit: and they that worship him must worship <em>him</em> in spirit and in truth.</span> The woman saith unto him, I know that Messias cometh, which is called Christ: when he is come, he will tell us all things. Jesus saith unto her, <span style=\"color:red;\">I that speak unto thee am <em>he.</em></span>".to_string());
        assert_eq!(res.1[0], ("God ".to_string(), Some(Color::Red), None));
        assert_eq!(res.1[1], ("is".to_string(), Some(Color::Red), Some(Styles::Italic)));
        assert_eq!(res.1[2], (" a Spirit: and they that worship him must worship ".to_string(), Some(Color::Red), None));
        assert_eq!(res.1[3], ("him".to_string(), Some(Color::Red), Some(Styles::Italic)));
        assert_eq!(res.1[4], (" in spirit and in truth.".to_string(), Some(Color::Red), None));
        assert_eq!(res.1[5], (" The woman saith unto him, I know that Messias cometh, which is called Christ: when he is come, he will tell us all things. Jesus saith unto her, ".to_string(), None, None));
        assert_eq!(res.1[6], ("I that speak unto thee am ".to_string(), Some(Color::Red), None));
        assert_eq!(res.1[7], ("he.".to_string(), Some(Color::Red), Some(Styles::Italic)));
    }

    #[test]
    fn multiverse_case_insensitive_book_name() {
        let mut bible_cli = Bible {
            book: Some("JoHn".to_string()),
            chapter: Some("4".to_string()),
            verse: Some("24-26".to_string()),
            input_option: InputOption::None,
        };
        let res = bible_cli.process().unwrap().unwrap();
        assert_eq!(res.0, "<span style=\"color:red;\">God <em>is</em> a Spirit: and they that worship him must worship <em>him</em> in spirit and in truth.</span> The woman saith unto him, I know that Messias cometh, which is called Christ: when he is come, he will tell us all things. Jesus saith unto her, <span style=\"color:red;\">I that speak unto thee am <em>he.</em></span>".to_string());
        assert_eq!(res.1[0], ("God ".to_string(), Some(Color::Red), None));
        assert_eq!(res.1[1], ("is".to_string(), Some(Color::Red), Some(Styles::Italic)));
        assert_eq!(res.1[2], (" a Spirit: and they that worship him must worship ".to_string(), Some(Color::Red), None));
        assert_eq!(res.1[3], ("him".to_string(), Some(Color::Red), Some(Styles::Italic)));
        assert_eq!(res.1[4], (" in spirit and in truth.".to_string(), Some(Color::Red), None));
        assert_eq!(res.1[5], (" The woman saith unto him, I know that Messias cometh, which is called Christ: when he is come, he will tell us all things. Jesus saith unto her, ".to_string(), None, None));
        assert_eq!(res.1[6], ("I that speak unto thee am ".to_string(), Some(Color::Red), None));
        assert_eq!(res.1[7], ("he.".to_string(), Some(Color::Red), Some(Styles::Italic)));
    }

    #[test]
    fn multiple_verses_with_colon() {
        let mut bible_cli = Bible {
            book: Some("John".to_string()),
            chapter: Some("4:24-26".to_string()),
            verse: None,
            input_option: InputOption::None,
        };
        let res = bible_cli.process().unwrap().unwrap();
        assert_eq!(res.0, "<span style=\"color:red;\">God <em>is</em> a Spirit: and they that worship him must worship <em>him</em> in spirit and in truth.</span> The woman saith unto him, I know that Messias cometh, which is called Christ: when he is come, he will tell us all things. Jesus saith unto her, <span style=\"color:red;\">I that speak unto thee am <em>he.</em></span>".to_string());
        assert_eq!(res.1[0], ("God ".to_string(), Some(Color::Red), None));
        assert_eq!(res.1[1], ("is".to_string(), Some(Color::Red), Some(Styles::Italic)));
        assert_eq!(res.1[2], (" a Spirit: and they that worship him must worship ".to_string(), Some(Color::Red), None));
        assert_eq!(res.1[3], ("him".to_string(), Some(Color::Red), Some(Styles::Italic)));
        assert_eq!(res.1[4], (" in spirit and in truth.".to_string(), Some(Color::Red), None));
        assert_eq!(res.1[5], (" The woman saith unto him, I know that Messias cometh, which is called Christ: when he is come, he will tell us all things. Jesus saith unto her, ".to_string(), None, None));
        assert_eq!(res.1[6], ("I that speak unto thee am ".to_string(), Some(Color::Red), None));
        assert_eq!(res.1[7], ("he.".to_string(), Some(Color::Red), Some(Styles::Italic)));
    }

    #[test]
    fn single_verse_with_colon() {
        let mut bible_cli = Bible {
            book: Some("John".to_string()),
            chapter: Some("4:24".to_string()),
            verse: None,
            input_option: InputOption::None,
        };
        let res = bible_cli.process().unwrap().unwrap();
        assert_eq!(res.0, "<span style=\"color:red;\">God <em>is</em> a Spirit: and they that worship him must worship <em>him</em> in spirit and in truth.</span>".to_string());
        assert_eq!(res.1[0], ("God ".to_string(), Some(Color::Red), None));
        assert_eq!(res.1[1], ("is".to_string(), Some(Color::Red), Some(Styles::Italic)));
        assert_eq!(res.1[2], (" a Spirit: and they that worship him must worship ".to_string(), Some(Color::Red), None));
        assert_eq!(res.1[3], ("him".to_string(), Some(Color::Red), Some(Styles::Italic)));
        assert_eq!(res.1[4], (" in spirit and in truth.".to_string(), Some(Color::Red), None));
    }


    // --------------- Negative tests ----------------

    #[test]
    fn no_verse() {
        let mut bible_cli = Bible {
            book: Some("John".to_string()),
            chapter: Some("4".to_string()),
            verse: None,
            input_option: InputOption::None,
        };
        let err = bible_cli.process().unwrap_err();
        match err {
            InputError::InvalidInput => (),
            _ => panic!(),
        }
    }

    #[test]
    fn no_chapter() {
        let mut bible_cli = Bible {
            book: Some("John".to_string()),
            chapter: None,
            verse: None,
            input_option: InputOption::None,
        };
        let err = bible_cli.process().unwrap_err();
        match err {
            InputError::InvalidInput => (),
            _ => panic!(),
        }
    }

    #[test]
    fn verse_with_no_chapter() {
        let mut bible_cli = Bible {
            book: Some("John".to_string()),
            chapter: None,
            verse: Some("4-5".to_string()),
            input_option: InputOption::None,
        };
        let err = bible_cli.process().unwrap_err();
        match err {
            InputError::InvalidInput => (),
            _ => panic!(),
        }
    }

    #[test]
    fn verse_doesnt_exist() {
        let mut bible_cli = Bible {
            book: Some("John".to_string()),
            chapter: Some("4:10000".to_string()),
            verse: None,
            input_option: InputOption::None,
        };
        let err = bible_cli.process().unwrap_err();
        match err {
            InputError::VerseNotFound => (),
            _ => panic!(),
        }
    }

    #[test]
    fn chapter_doesnt_exist() {
        let mut bible_cli = Bible {
            book: Some("John".to_string()),
            chapter: Some("10000:4".to_string()),
            verse: None,
            input_option: InputOption::None,
        };
        let err = bible_cli.process().unwrap_err();
        match err {
            InputError::ChapterNotFound => (),
            _ => panic!(),
        }
    }

    #[test]
    fn verse_and_chapter_dont_exist() {
        let mut bible_cli = Bible {
            book: Some("John".to_string()),
            chapter: Some("10000:10000".to_string()),
            verse: None,
            input_option: InputOption::None,
        };
        let err = bible_cli.process().unwrap_err();
        match err {
            InputError::ChapterNotFound => (),
            _ => panic!(),
        }
    }

    #[test]
    fn verse_and_chapter_dont_exist_with_range() {
        let mut bible_cli = Bible {
            book: Some("John".to_string()),
            chapter: Some("10000:10000-1000001".to_string()),
            verse: None,
            input_option: InputOption::None,
        };
        let err = bible_cli.process().unwrap_err();
        match err {
            InputError::ChapterNotFound => (),
            _ => panic!(),
        }
    }

    #[test]
    fn book_doesnt_exist() {
        let mut bible_cli = Bible {
            book: Some("Jhne".to_string()),
            chapter: Some("1:1".to_string()),
            verse: None,
            input_option: InputOption::None,
        };
        let err = bible_cli.process().unwrap_err();
        match err {
            InputError::BookNotFound => (),
            _ => panic!(),
        }
    }

    #[test]
    fn invalid_verse_range() {
        let mut bible_cli = Bible {
            book: Some("John".to_string()),
            chapter: Some("1:5-1".to_string()),
            verse: None,
            input_option: InputOption::None,
        };
        let err = bible_cli.process().unwrap_err();
        match err {
            InputError::InvalidVerseRange => (),
            _ => panic!(),
        }
    }
}
