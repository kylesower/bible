use bible::handle_error;
use clap::Parser;
use crate::bible::Bible;
pub mod bible;
mod integration_tests;


fn main() {
    let mut bible_cli = Bible::parse();
    let res = bible_cli.process();
    handle_error(&bible_cli, res);
}
