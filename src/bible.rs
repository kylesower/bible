use clap::Parser;
use colored::{Color, Colorize, Styles};
use json::{parse, JsonValue::Null};
use rand::prelude::*;
use regex::Regex;

static ABBREVS: [(&str, &str); 66] = [
    ("JOS", "Joshua"),
    ("2CO", "2 Corinthians"),
    ("ACT", "Acts"),
    ("PRO", "Proverbs"),
    ("JDG", "Judges"),
    ("2JN", "2 John"),
    ("1TH", "1 Thessalonians"),
    ("LEV", "Leviticus"),
    ("1PE", "1 Peter"),
    ("HAB", "Habakkuk"),
    ("JUD", "Jude"),
    ("1SA", "1 Samuel"),
    ("PHM", "Philemon"),
    ("GEN", "Genesis"),
    ("LUK", "Luke"),
    ("2CH", "2 Chronicles"),
    ("2TI", "2 Timothy"),
    ("1CO", "1 Corinthians"),
    ("EST", "Esther"),
    ("ISA", "Isaiah"),
    ("JER", "Jeremiah"),
    ("2KI", "2 Kings"),
    ("HOS", "Hosea"),
    ("AMO", "Amos"),
    ("2PE", "2 Peter"),
    ("PHP", "Philippians"),
    ("MAT", "Matthew"),
    ("OBA", "Obadiah"),
    ("JHN", "John"),
    ("HEB", "Hebrews"),
    ("DEU", "Deuteronomy"),
    ("MAL", "Malachi"),
    ("NEH", "Nehemiah"),
    ("1JN", "1 John"),
    ("3JN", "3 John"),
    ("GAL", "Galatians"),
    ("1CH", "1 Chronicles"),
    ("ZEC", "Zechariah"),
    ("2SA", "2 Samuel"),
    ("DAN", "Daniel"),
    ("1KI", "1 Kings"),
    ("NUM", "Numbers"),
    ("EPH", "Ephesians"),
    ("JON", "Jonah"),
    ("RUT", "Ruth"),
    ("LAM", "Lamentations"),
    ("JAS", "James"),
    ("REV", "Revelation"),
    ("JOB", "Job"),
    ("HAG", "Haggai"),
    ("ROM", "Romans"),
    ("EZR", "Ezra"),
    ("2TH", "2 Thessalonians"),
    ("EXO", "Exodus"),
    ("EZK", "Ezekiel"),
    ("MRK", "Mark"),
    ("TIT", "Titus"),
    ("ZEP", "Zephaniah"),
    ("MIC", "Micah"),
    ("PSA", "Psalms"),
    ("SNG", "Song of Solomon"),
    ("1TI", "1 Timothy"),
    ("JOL", "Joel"),
    ("ECC", "Ecclesiastes"),
    ("NAM", "Nahum"),
    ("COL", "Colossians"),
];

#[derive(Default, Debug)]
pub enum InputOption {
    Selected,
    Invalid,
    #[default]
    None,
}

#[derive(Default, Debug)]
pub enum InputError {
    BookNotFound,
    ChapterNotFound,
    VerseNotFound,
    InvalidInput,
    InvalidVerseRange,
    #[default]
    None,
}

pub fn handle_error(b: &Bible, res: Result<Option<(String, Vec<StyledText>)>, InputError>) {
    if res.is_ok() { return }
    match res.unwrap_err() {
        InputError::BookNotFound => eprintln!("Book '{}' not found. Available abbreviations and books:\n{:?}", b.book.as_ref().unwrap(), ABBREVS),
        InputError::InvalidInput => eprintln!("Invalid input. Make sure you separate the chapter and verse with a colon or space. If you want multiple verses, separate the verses with a hyphen."),
        InputError::ChapterNotFound => eprintln!("Chapter '{}' not found in {}.", b.chapter.as_ref().unwrap(), b.book.as_ref().unwrap()),
        InputError::VerseNotFound => eprintln!("Verse(s) '{}' not found in {} {}.", b.verse.as_ref().unwrap(), b.book.as_ref().unwrap(), b.chapter.as_ref().unwrap()),
        InputError::InvalidVerseRange => eprintln!("Verse range '{}' is invalid.", b.verse.as_ref().unwrap()),
        _ => (),
    }
}

#[derive(Debug, Parser, Default)]
#[command(author, version, about, long_about = None)]
pub struct Bible {
    pub book: Option<String>,
    pub chapter: Option<String>,
    pub verse: Option<String>,
    #[clap(skip)]
    pub input_option: InputOption,
}

impl Bible {
    pub fn process(&mut self) -> Result<Option<(String, Vec<StyledText>)>, InputError> {
        self.sanitize_input()?;
        match self.input_option {
            InputOption::None => self.print_random_verse(),
            InputOption::Selected => self.print_selected_verse(),
            InputOption::Invalid => Err(InputError::InvalidInput),
        }
    }

    fn sanitize_input(&mut self) -> Result<(), InputError> {
        let chap_pat = Regex::new(r"\d+").unwrap();
        let verse_pat = Regex::new(r"(\d+-\d+|\d+)").unwrap();
        let chap_and_verse_pat = Regex::new(r"(\d+):(\d+-\d+|\d+)").unwrap();
        if self.book.is_some() {
            let mut book = self.book.as_ref().unwrap().clone();
            if ['1', '2', '3'].contains(&book.chars().next().unwrap()) && book.len() > 3 {
                self.book = Some(format!("{} {}", book.remove(0), book));
            }
        }
        if self.book.is_none() && self.chapter.is_none() && self.verse.is_none() {
            self.input_option = InputOption::None;
            Ok(())
        } else if self.book.is_some() && self.chapter.is_some() && self.verse.is_none() {
            let chapter_num = self.chapter.as_ref().unwrap().clone();
            if let Some(chap_and_verse) = chap_and_verse_pat.captures(&chapter_num) {
                self.chapter = Some(chap_and_verse[1].to_string());
                self.verse = Some(chap_and_verse[2].to_string());
                self.input_option = InputOption::Selected;
                return Ok(())
            }
            self.input_option = InputOption::Invalid;
            return Err(InputError::InvalidInput);
        } else if self.book.is_some() && self.chapter.is_some() && self.verse.is_some() {
            let chapter_num = self.chapter.as_ref().unwrap().clone();
            let verse_num = self.verse.as_ref().unwrap().clone();
            if let Some(verse_mat) = verse_pat.captures(&verse_num) {
                if let Some(chap_mat) = chap_pat.captures(&chapter_num) {
                    if verse_mat.len() == 2 && chap_mat.len() == 1 {
                        self.input_option = InputOption::Selected;
                        return Ok(())
                    }
                }
            }
            self.input_option = InputOption::Invalid;
            return Err(InputError::InvalidInput)
        } else {
            self.input_option = InputOption::Invalid;
            return Err(InputError::InvalidInput)
        }
    }

    fn print_selected_verse(&self) -> Result<Option<(String, Vec<StyledText>)>, InputError> {
        let mut book_name = self.book.as_ref().unwrap().clone();
        let chapter_num = self.chapter.as_ref().unwrap().clone();
        let verse_num = self.verse.as_ref().unwrap().clone();
        let mut bible = parse(include_str!("kjv_bible.json")).unwrap();

        if !bible.has_key(&book_name) {
            let mut match_found = false;
            for abbrev in ABBREVS {
                if abbrev.0 == book_name.to_uppercase()
                    || abbrev.1.to_lowercase() == book_name.to_lowercase()
                {
                    match_found = true;
                    book_name = abbrev.1.to_string();
                    break;
                }
            }
            if !match_found {
                return Err(InputError::BookNotFound)
            }
        }

        let book = bible.remove(&book_name);
        let chapter = &book[&chapter_num];
        if chapter == &Null {
            return Err(InputError::ChapterNotFound)
        }
        let mut text;
        if verse_num.contains('-') {
            let verses: Vec<&str> = verse_num.split('-').collect();
            let start_verse: u16 = verses[0].parse().expect("Must enter a number.");
            let stop_verse: u16 = verses[1].parse().expect("Must enter a number.");
            if start_verse >= stop_verse {
                return Err(InputError::InvalidVerseRange)
            }

            let first = &chapter[start_verse.to_string()];
            if first == &Null {
                return Err(InputError::VerseNotFound);
            } else {
                text = first.to_string()
            }

            for i in (start_verse + 1)..=stop_verse {
                let next_verse = &chapter[i.to_string()];
                if next_verse == &Null {
                    return Err(InputError::VerseNotFound);
                }
                text = format!("{} {}", text, next_verse);
            }
        } else {
            let first = &chapter[&verse_num];
            if first == &Null {
                return Err(InputError::VerseNotFound);
            }
            text = first.to_string()
        }

        let styles = get_styled_text(&text);
        print!("\"");
        for item in styles.iter() {
            if item.1.is_some() {
                if item.2.is_some() {
                    print!("{}", item.0.red().italic());
                } else {
                    print!("{}", item.0.red());
                }
            } else if item.2.is_some() {
                print!("{}", item.0.italic());
            } else {
                print!("{}", item.0.normal());
            }
        }
        println!("\" ~ {} {}:{}", book_name, chapter_num, verse_num);
        Ok(Some((text, styles)))
    }

    fn print_random_verse(&self) -> Result<Option<(String, Vec<StyledText>)>, InputError> {
        let bible = parse(include_str!("kjv_bible.json")).unwrap();
        let num_books = bible.len();
        let book_num = rand::thread_rng().gen_range(0..num_books);
        let book = bible.entries().nth(book_num).unwrap();
        let num_chapters = book.1.len();
        let chapter_num = rand::thread_rng().gen_range(1..num_chapters + 1);
        let chapter = &book.1[chapter_num.to_string()];
        let num_verses = chapter.len();
        let mut verse_num = rand::thread_rng().gen_range(1..num_verses + 1);
        let init_verse = verse_num;
        let chapter = &book.1[chapter_num.to_string()];
        let verse = &chapter[verse_num.to_string()];
        let mut text = verse.to_string();

        while !text.ends_with('.')
            && !text.ends_with('!')
            && !text.ends_with('?')
            && !text.ends_with(".</span>")
            && !text.ends_with("!</span>")
            && !text.ends_with("?</span>")
            && !text.ends_with(".</em>")
            && !text.ends_with("!</em>")
            && !text.ends_with("?</em>")
        {
            verse_num += 1;
            text = format!("{} {}", text, &chapter[verse_num.to_string()]);
        }
        let verse_num_str = if init_verse != verse_num {
            format!("{}-{}", init_verse, verse_num)
        } else {
            format!("{}", verse_num)
        };

        let styles = get_styled_text(&text);
        print!("\"");
        for item in styles.iter() {
            if item.1.is_some() {
                if item.2.is_some() {
                    print!("{}", item.0.red().italic());
                } else {
                    print!("{}", item.0.red());
                }
            } else if item.2.is_some() {
                print!("{}", item.0.italic());
            } else {
                print!("{}", item.0.normal());
            }
        }
        println!("\" ~ {} {}:{}", book.0, chapter_num, verse_num_str);
        Ok(Some((text, styles)))
    }
}

pub type StyledText = (String, Option<Color>, Option<Styles>);

pub fn get_styled_text(text: &str) -> Vec<StyledText> {
    let mut styles: Vec<StyledText> = Vec::new();
    let mut current_str = String::new();
    let mut italic = false;
    let mut red = false;
    let mut push_char = true;
    let mut read_style = false;
    let mut stop_style = false;
    for char in text.chars() {
        if read_style {
            read_style = false;
            if char == 's' && !stop_style {
                if !current_str.is_empty() {
                    styles.push((current_str.clone(),
                                 None,
                                 if italic {Some(Styles::Italic)} else {None}));
                    current_str = "".to_string();
                }
                red = true;
            } else if char == 'e' && !stop_style {
                if !current_str.is_empty() {
                    styles.push((current_str.clone(),
                                 if red {Some(Color::Red)} else {None},
                                 None));
                    current_str = "".to_string();
                }
                italic = true;
            } else if char == '/' {
                stop_style = true;
                read_style = true;
            } else if char == 's' && stop_style {
                if !current_str.is_empty() {
                    styles.push((current_str.clone(),
                                 Some(Color::Red),
                                 if italic {Some(Styles::Italic)} else {None}));
                    current_str = "".to_string();
                }
                stop_style = false;
                red = false;
            } else if char == 'e' && stop_style {
                if !current_str.is_empty() {
                    styles.push((current_str.clone(),
                                 if red {Some(Color::Red)} else {None},
                                 Some(Styles::Italic)));
                    current_str = "".to_string();
                }
                stop_style = false;
                italic = false;
            }
        }
        if char == '<' {
            push_char = false;
            read_style = true;
        } else if char == '>' {
            push_char = true;
        }
        if push_char && char != '>' {
            current_str.push(char);
        }
    }
    if !current_str.is_empty() {
        styles.push((current_str,
                     if red {Some(Color::Red)} else {None},
                     if italic {Some(Styles::Italic)} else {None}));
    }
    styles
}

